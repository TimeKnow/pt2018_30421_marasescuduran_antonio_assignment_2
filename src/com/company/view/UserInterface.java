package com.company.view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class UserInterface extends JFrame {
    private static final int HEIGHT=500;
    private static final int WiDTH=700;
    private int noQueues=2;
    private JButton start;
    private JPanel mainPanel;
    private JPanel inputPanel;
    private JPanel outputPanel;
    private JPanel dataPanel;
    private List<JSpinner> spinnerList=new ArrayList<>();
    private List<JLabel> queuesList = new ArrayList<>();
    private List<JLabel> dataList = new ArrayList<>();
    String[] labels = new String[]{"Minimal Arrival:","Maximum Arrival:","Minimum Service:","Maximum Service:"
            ,"Number Queues:","Number Clients:","Simulation Time:"};
    String[] labelData = new String[]{"Average Waiting Time:","Average Service Time:","Empty Queues:","Peak Time:"
            ,"Simulation Time:"};
    public UserInterface(int noQueues){
        this.noQueues = noQueues;
        draw();
    }

    private final void draw(){
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(this.mainPanel, 1));
        mainPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);

        inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel,BoxLayout.Y_AXIS));
        inputPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        //inputPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);

        outputPanel = new JPanel();
        outputPanel.setLayout(new BoxLayout(outputPanel,BoxLayout.Y_AXIS));
        outputPanel.setBorder(new EmptyBorder(0,0,0,0));
        outputPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        outputPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        //outputPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);

        dataPanel = new JPanel();
        dataPanel.setLayout(new BoxLayout(dataPanel,BoxLayout.Y_AXIS));
        dataPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
       // dataPanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);

        for(int i=0;i<7;i++) {
            int k=0;
            if(i!=0 && i!=2)
                k=1;
            SpinnerNumberModel spinnerModel = new SpinnerNumberModel(k, k, 30, 1);
            JSpinner spinner = new JSpinner(spinnerModel);
            JLabel jl = new JLabel(labels[i]);
            spinner.setAlignmentX(JSpinner.LEFT_ALIGNMENT);
            jl.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            jl.setHorizontalAlignment(JLabel.LEFT);
            jl.setBorder(new EmptyBorder(0,0,0,5));
            JPanel jp = new JPanel();
            jp.setAlignmentX(JPanel.LEFT_ALIGNMENT);
            jp.setLayout(new BoxLayout(jp,BoxLayout.X_AXIS));
            jp.add(jl);
            jp.add(spinner);
            jp.setBorder(new EmptyBorder(5,10,5,10));
            spinnerList.add(spinner);
            inputPanel.add(jp);
        }
        JPanel jbutton = new JPanel();
        jbutton.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        jbutton.setLayout(new FlowLayout());
        start = new JButton();
        start.setAlignmentX(0.0f);
        start.setText("Start Simulation");
        jbutton.add(start);
        //inputPanel.add(jbutton);

        drawOutputPanel();

        for(int i=0;i<5;i++) {
            JPanel jpanel = new JPanel();
            jpanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
            jpanel.setLayout(new BoxLayout(jpanel,BoxLayout.X_AXIS));
            JLabel j = new JLabel();
            jpanel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
            j.setHorizontalAlignment(JLabel.LEFT);
            j.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            //j.setFont(new Font("Arial", Font.PLAIN, 14));
            j.setHorizontalAlignment(JLabel.LEFT);
            j.setText("0");
            JLabel tj = new JLabel(labelData[i]);
            tj.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            tj.setHorizontalAlignment(JLabel.LEFT);
            dataList.add(j);
            jpanel.add(tj);
            jpanel.add(j);
            dataPanel.add(jpanel);
        }

        JPanel inputData = new JPanel();
        inputData.setAlignmentX(JPanel.LEFT_ALIGNMENT);
        inputData.setLayout(new FlowLayout());
        inputData.add(inputPanel);
        //inputData.add(new JSeparator(SwingConstants.VERTICAL));
        inputData.add(dataPanel);
        mainPanel.add(inputData);
        //mainPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
        //mainPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
        mainPanel.add(jbutton);
        //mainPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
        mainPanel.add(outputPanel);

        this.setContentPane(this.mainPanel);
        this.setTitle("Queue Simulation");
        this.setSize(WiDTH, HEIGHT);
        this.setLocationRelativeTo((Component)null);
        this.setDefaultCloseOperation(3);
    }

    public void drawOutputPanel(){
        outputPanel.removeAll();
        queuesList.clear();
        for(int i=0;i<noQueues;i++) {
            JLabel l = new JLabel();
            JLabel l2 = new JLabel("Queue"+i+": ");
            l.setHorizontalAlignment(JLabel.LEFT);
            l.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            l2.setHorizontalAlignment(JLabel.LEFT);
            l2.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            //l.setText("\"t1 t2 t3 t4 t5 t6\"");
            l.setEnabled(false);
            JPanel p = new JPanel();
            p.setAlignmentX(JPanel.LEFT_ALIGNMENT);

            p.setLayout(new BoxLayout(p,BoxLayout.X_AXIS));
            queuesList.add(l);
            p.add(l2);
            p.add(l);
            outputPanel.add(p);
        }
        this.revalidate();
        this.repaint();
    }

    public JButton getStart() {
        return start;
    }

    public void setStart(JButton start) {
        this.start = start;
    }

    public List<JSpinner> getSpinnerList() {
        return spinnerList;
    }

    public void setSpinnerList(List<JSpinner> spinnerList) {
        this.spinnerList = spinnerList;
    }

    public List<JLabel> getQueuesList() {
        return queuesList;
    }

    public void setQueuesList(List<JLabel> queuesList) {
        this.queuesList = queuesList;
    }

    public List<JLabel> getDataList() {
        return dataList;
    }

    public void setDataList(List<JLabel> dataList) {
        this.dataList = dataList;
    }

    public int getNoQueues() {
        return noQueues;
    }

    public void setNoQueues(int noQueues) {
        this.noQueues = noQueues;
    }
}
