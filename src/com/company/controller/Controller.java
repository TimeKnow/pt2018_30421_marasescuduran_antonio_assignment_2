package com.company.controller;

import com.company.model.SimulationManager;
import com.company.view.UserInterface;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener{

    private UserInterface userInterface;
    private SimulationManager simulationManager;

    public Controller(UserInterface userInterface, SimulationManager simulationManager) {
        this.userInterface = userInterface;
        this.simulationManager = simulationManager;
        userInterface.getStart().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(userInterface.getStart()==e.getSource())
            startSimulation();
    }

    private void startSimulation(){
        if(!simulationManager.isOnline()) {
            int minArriv = (int) this.userInterface.getSpinnerList().get(0).getValue();
            int maxArriv = (int) this.userInterface.getSpinnerList().get(1).getValue();
            int minServi = (int) this.userInterface.getSpinnerList().get(2).getValue();
            int maxServi = (int) this.userInterface.getSpinnerList().get(3).getValue();
            int noQueues = (int) this.userInterface.getSpinnerList().get(4).getValue();
            int noClient = (int) this.userInterface.getSpinnerList().get(5).getValue();
            int simuTime = (int) this.userInterface.getSpinnerList().get(6).getValue();
            if(minArriv > maxArriv || minServi > maxServi || simuTime < minArriv || simuTime < minServi) {
                JOptionPane.showMessageDialog(null, "Wrong simulation values!");
                return;
            }
            userInterface.setNoQueues(noQueues);
            userInterface.drawOutputPanel();

            simulationManager.setMinArrivalTime(minArriv);
            simulationManager.setMaxArrivalTime(maxArriv);
            simulationManager.setMinProcessingTime(minServi);
            simulationManager.setMaxProcessingTime(maxServi);
            simulationManager.setNumberOfServers(noQueues);
            simulationManager.setNumberOfClients(noClient);
            simulationManager.setTimeLimit(simuTime);
            simulationManager.setOnline(true);
            simulationManager.reset(true);
            Thread t =new Thread(simulationManager);
            t.start();
        }

    }
}
