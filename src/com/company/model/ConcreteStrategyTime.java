package com.company.model;

import java.util.Iterator;
import java.util.List;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addTask(List<Server> servers, Task t) {
        if(servers.size()==0)
            return;
        Server minimalServer=servers.get(0);
        int minimumTime = Integer.MAX_VALUE;
        int selectedIndex = 0;
        int index = 0 ;
        Iterator<Server> iter = servers.iterator();
        while(iter.hasNext()) {
            Server s = iter.next();

            if (minimumTime > s.getWaitingPeriod().get()) {
                minimumTime = s.getWaitingPeriod().get();
                minimalServer = s;
                selectedIndex = index;
            }
            index++;
        }
        t.setServerIndex(selectedIndex);
        minimalServer.addTask(t);
    }
}
