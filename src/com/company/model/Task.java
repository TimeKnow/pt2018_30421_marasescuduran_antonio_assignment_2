package com.company.model;

import java.util.Random;

public class Task {
    private String name;
    private int arrivalTime;
    private int processingTime;
    private int waitingTime = 0;
    private int finalTime = -1;
    private int serverIndex = 0;
    public Task(String name, int arrivalTime, int processingTime) {
        this.name = name;
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
    }

    public Task() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getFinalTime() {
        return finalTime;
    }

    public void setFinalTime(int finalTime) {
        this.finalTime = finalTime;
    }

    public int getServerIndex() {
        return serverIndex;
    }

    public void setServerIndex(int serverIndex) {
        this.serverIndex = serverIndex;
    }

    /**
     *  Static functions that returns a random task based on the function parameters
     * @param maxPrc maximum processing time
     * @param minPrc minimum processing time
     * @param timeLimit server time interval
     * @param index task index
     * @param maxArriv maximum arrival time
     * @param minArriv minimum arrival time
     * @return a new random Task
     */
    public static Task generateTask(int maxPrc, int minPrc, int timeLimit, int index, int maxArriv, int minArriv){
        Random r =new Random();
        int processingTime = r.nextInt(maxPrc-minPrc)+minPrc+1;
        int arrivalTime = r.nextInt(maxArriv-minArriv)+minArriv+1;
        return new Task("T"+String.valueOf(index),arrivalTime,processingTime);
    }

    /**
     * Function that returns true if the current task is equal to an object o
     * @param o the object which our current task will be compared to
     * @return true if they are equal false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;

        Task task = (Task) o;

        return name.equals(task.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
