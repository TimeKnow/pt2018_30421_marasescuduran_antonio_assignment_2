package com.company.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class ProgramLog {
    private static List<String> logTasks = new ArrayList<>();
    private static List<String> logServers = new ArrayList<>();
    private static List<String> logSimulation = new ArrayList<>();
    public static void logMessageTask(String Message){
        if(Message!=null)
        logTasks.add(Message+"\n");
    }
    public static void logMessageServer(String Message){
        if(Message!=null)
        logServers.add(Message+"\n");
    }
    public static void logMessageSimulation(String Message){
        if(Message!=null)
        logSimulation.add(Message+"\n");
    }
    public static void getLog(){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("programLog.txt",false));
            writer.write("\nTask Data\n\n");
            for(String s : logTasks)
                writer.write(s);
            writer.write("\n\nServer Data\n\n");
            for(String s : logServers)
                writer.write(s);
            writer.write("\n\nSimulation Data\n\n");
            for(String s : logSimulation)
                writer.write(s);
            writer.close();
        }
        catch (Exception e){

        }
    }
}
