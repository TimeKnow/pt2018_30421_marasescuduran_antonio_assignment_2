package com.company.model;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;
    private List<Thread> serverThreads;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;

    public Scheduler(int maxNoServers, int maxTasksPerServer) {
        this.maxNoServers = maxNoServers;
        this.maxTasksPerServer = maxTasksPerServer;
        servers = new ArrayList<>();
        serverThreads = new ArrayList<>();
        for(int i=0; i<maxNoServers;i++){
            Server s=new Server("Server"+String.valueOf(i),maxTasksPerServer);
            servers.add(s);
        }
    }

    public void startServers(){
        for(Server s : servers){
            Thread t=new Thread(s);
            serverThreads.add(t);
            t.start();
        }
    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy==SelectionPolicy.SHORTEST_TIME)
            strategy = new ConcreteStrategyTime();
        if(policy==SelectionPolicy.SHORTEST_QUEUE)
            strategy = new ConcreteStrategyQueue();
    }

    public void dispatchTask(Task t){

        strategy.addTask(servers,t);
    }

    public void closeServers(){

        for(Server s : servers) {
            s.setOnline(false);
        }
        for(Thread t : serverThreads)
            if(t.isAlive())
                t.interrupt();

    }

    public List<Server> getServers() {
        return servers;
    }

    public void setServers(List<Server> servers) {
        this.servers = servers;
    }
}
