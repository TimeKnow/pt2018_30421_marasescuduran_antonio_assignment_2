package com.company.model;

public enum SelectionPolicy {
    SHORTEST_QUEUE, SHORTEST_TIME
}
