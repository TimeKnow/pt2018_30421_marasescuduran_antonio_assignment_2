package com.company.model;


import java.util.ArrayList;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    private String name;
    private boolean isOnline=true;
    private ArrayBlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private AtomicInteger averageService = new AtomicInteger(0);
    private AtomicInteger lastFinalTime = new AtomicInteger(Integer.MAX_VALUE);

    public Server(String name,int maxTasksPerServer) {
        this.name = name;
        tasks = new ArrayBlockingQueue<>(maxTasksPerServer);
        waitingPeriod = new AtomicInteger(0);
    }

    public Server(String name, ArrayBlockingQueue<Task> tasks, AtomicInteger waitingPeriod) {
        this.name = name;
        this.tasks = tasks;
        this.waitingPeriod = waitingPeriod;
    }

    /**
     * Adds a new task to our server task queue and sets the task waiting time
     * @param newTask the new task to be added
     */
    public void addTask(Task newTask){
        tasks.add(newTask);
        newTask.setWaitingTime(waitingPeriod.get());
        waitingPeriod.getAndAdd(newTask.getProcessingTime());
    }

    /**
     * Runs our server continously while the boolean isOnline is true and process the tasks from the queue
     */
    @Override
    public void run() {
        while(isOnline){
            try {

                Task currentTask = tasks.take();
                printTaskData(currentTask);

                averageService.set(currentTask.getProcessingTime());

                ProgramLog.logMessageServer("General Waiting Period "+" of "+ name +": "+waitingPeriod.get());
                if(Thread.currentThread().isInterrupted())
                    break;
                Thread.sleep(currentTask.getProcessingTime()*1000);
                waitingPeriod.getAndAdd(-currentTask.getProcessingTime());
            }
            catch (InterruptedException ie){
                ProgramLog.logMessageServer(ie.getMessage());
            }

        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns a list of tasks representing our queue
     * @return
     */
    public List<Task> getTasks() {

        List<Task> returnData = new ArrayList<>();
        Object[] objects =  tasks.toArray();
        for(Object o : objects)
            returnData.add((Task)o);
        return returnData;
    }

    public void setTasks(ArrayBlockingQueue<Task> tasks) {
        this.tasks = tasks;
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    /**
     * Messages the log with information regarding a task
     * @param t Task
     */
    private void printTaskData(Task t){

        ProgramLog.logMessageServer(t.getName()+" of "+ name +" Arrived: "+t.getArrivalTime());
        int processingStart = t.getArrivalTime()+t.getWaitingTime();
        if(processingStart>lastFinalTime.get())
            processingStart = lastFinalTime.get();
        if(processingStart < t.getArrivalTime())
            processingStart = t.getArrivalTime();
        ProgramLog.logMessageServer(t.getName()+" of "+ name +" the processing started at  "
                +processingStart);
        int finishTime = t.getProcessingTime()+processingStart;
        t.setFinalTime(finishTime);
        lastFinalTime.set(finishTime);
        ProgramLog.logMessageServer(t.getName()+" of "+ name +" Final Time: "+finishTime);

    }

    public AtomicInteger getAverageService() {
        return averageService;
    }
}
