package com.company.model;

import com.company.view.UserInterface;

import javax.swing.*;
import java.util.*;

public class SimulationManager implements Runnable{
    private int timeLimit=15;
    private int maxProcessingTime=5;
    private int minProcessingTime=2;
    private int maxArrivalTime=8;
    private int minArrivalTime=2;
    private int numberOfServers=2;
    private int numberOfClients=5;

    private int peakTime=Integer.MIN_VALUE;
    private int peakTimeValue=Integer.MIN_VALUE;
    private int avgWaitTime=0;
    private  int avgServiceTime=0;
    private int avgEmptyQueue=0;
    private boolean online=false;
    int currentTime=0;

    private SelectionPolicy selectionPolicy=SelectionPolicy.SHORTEST_TIME;
    private UserInterface frame;
    private Scheduler scheduler;
    private List<Task> generatedTasks;

    public SimulationManager(UserInterface frame) {
        this.frame = frame;
        reset(false);
    }

    public SimulationManager(){
       reset(false);
    }

    /**
     * Generates a new list of random task to be
     * used for processing in our simulation
     */
    private void generateTask(){
        generatedTasks = new ArrayList<>();
        for(int i=0;i<numberOfClients;i++)
            generatedTasks.add(Task.generateTask(maxProcessingTime,minProcessingTime,timeLimit,i,maxArrivalTime,minArrivalTime));
        Collections.sort(generatedTasks, (o1, o2) -> Integer.compare(o1.getArrivalTime(),o2.getArrivalTime()));
    }

    /**
     * Resets the scheduler and generates new task for our simulation
     * @param willLog boolean value if our task data will be recorded in the log
     */
    public void reset(boolean willLog){
        scheduler = new Scheduler(numberOfServers,numberOfClients);
        scheduler.changeStrategy(selectionPolicy);

        scheduler.startServers();
        generateTask();
        if(willLog)
        for(Task t : generatedTasks)
            ProgramLog.logMessageTask("Task"+t.getName()+" Arrival Time:"+t.getArrivalTime() + " Processing Time:"+ t.getProcessingTime());
    }

    /**
     * Method that stops our simulation
     */
    public void stop(){
        currentTime = timeLimit+1;
    }

    /**
     * Method that implements our simulation by creating and artificial time simulation
     * and processing tasks depending on the arrival time and finish time
     * Moreover, it updates the user interface with relevant data such as peak time, average waiting time...
     */
    @Override
    public void run() {
        currentTime = 0;
        List<Server> serverList = scheduler.getServers();
        while(currentTime<=timeLimit){

            Iterator<Task> iter = generatedTasks.iterator();
            while(iter.hasNext()){
                Task t = iter.next();
                if(t.getArrivalTime()==currentTime){
                    scheduler.dispatchTask(t);
                    String s = frame.getQueuesList().get(t.getServerIndex()).getText();
                    s+=t.getName()+" ";
                    frame.getQueuesList().get(t.getServerIndex()).setText(s);

                }
                if(t.getFinalTime()==currentTime)
                {
                    String newData="";
                    String queueData = frame.getQueuesList().get(t.getServerIndex()).getText();
                    String[] data = queueData.split(" ");
                    for(String s : data)
                        if(!s.trim().equals(t.getName()))
                            newData+=s.trim()+" ";
                    frame.getQueuesList().get(t.getServerIndex()).setText(newData);
                    iter.remove();
                }
            }


            int average=0;
            avgEmptyQueue=0;
            avgServiceTime=0;
            for(Server s : serverList)
            {
                average +=s.getWaitingPeriod().get();
                avgServiceTime += s.getAverageService().get();
            }

            for(JLabel l : frame.getQueuesList())
                if(l.getText().trim().equals(""))
                    avgEmptyQueue++;

            average /= serverList.size();

            avgWaitTime=average;
            if(peakTimeValue < average){
                peakTimeValue = average;
                peakTime = currentTime;
            }

            ProgramLog.logMessageSimulation("Simulation Time "+currentTime+" : Peak Time:"+peakTime+" AverageWaitTime:"+avgWaitTime+
                    " AverageServiceTime:"+avgServiceTime + " AverageEmptyQueue:"+avgEmptyQueue);
            frame.getDataList().get(0).setText(String.valueOf(avgWaitTime));
            frame.getDataList().get(1).setText(String.valueOf(avgServiceTime));
            frame.getDataList().get(2).setText(String.valueOf(avgEmptyQueue));
            frame.getDataList().get(3).setText(String.valueOf(peakTime));
            frame.getDataList().get(4).setText(String.valueOf(currentTime));


            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException ie){
            }

            currentTime++;
        }
        for(int i=0;i<numberOfServers;i++)
            frame.getQueuesList().get(i).setText("");
        scheduler.closeServers();

        ProgramLog.getLog();
        this.online=false;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public int getMaxProcessingTime() {
        return maxProcessingTime;
    }

    public void setMaxProcessingTime(int maxProcessingTime) {
        this.maxProcessingTime = maxProcessingTime;
    }

    public int getMinProcessingTime() {
        return minProcessingTime;
    }

    public void setMinProcessingTime(int minProcessingTime) {
        this.minProcessingTime = minProcessingTime;
    }

    public int getMaxArrivalTime() {
        return maxArrivalTime;
    }

    public void setMaxArrivalTime(int maxArrivalTime) {
        this.maxArrivalTime = maxArrivalTime;
    }

    public int getMinArrivalTime() {
        return minArrivalTime;
    }

    public void setMinArrivalTime(int minArrivalTime) {
        this.minArrivalTime = minArrivalTime;
    }

    public int getNumberOfServers() {
        return numberOfServers;
    }

    public void setNumberOfServers(int numberOfServers) {
        this.numberOfServers = numberOfServers;
    }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public void setNumberOfClients(int numberOfClients) {
        this.numberOfClients = numberOfClients;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
