package com.company;

import com.company.controller.Controller;
import com.company.model.SimulationManager;
import com.company.view.UserInterface;

public class Main {

    public static void main(String[] args) {

        UserInterface ui = new UserInterface(6);
        ui.setVisible(true);
        SimulationManager sm = new SimulationManager(ui);
        Controller c = new Controller(ui,sm);

    }

}
